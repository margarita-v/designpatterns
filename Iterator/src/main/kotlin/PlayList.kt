class PlayList {

    private val songs: ArrayList<Song> = ArrayList()

    init {
        addSong("Heaven upside down", "Marilyn Manson")
        addSong("Custer", "Slipknot")
        addSong("What I've done", "Linkin Park")
    }

    fun getSize(): Int = songs.size

    operator fun get(position: Int): Song = songs[position]

    private fun addSong(name: String, author: String): Boolean = songs.add(Song(name, author))
}