class PlayListIterator(private val playList: PlayList) : Iterator {

    private var currentIndex = 0

    override fun getNext(): Song? {
        val result = if (hasNext()) playList[currentIndex] else null
        currentIndex++
        return result
    }

    override fun hasNext(): Boolean = currentIndex < playList.getSize()
}