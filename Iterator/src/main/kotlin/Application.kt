fun main(args: Array<String>) {
    val playList = PlayList()
    val iterator = PlayListIterator(playList)

    while (iterator.hasNext()) {
        println(iterator.getNext())
    }
}