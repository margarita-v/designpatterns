interface Iterator {

    fun getNext(): Song?

    fun hasNext(): Boolean
}