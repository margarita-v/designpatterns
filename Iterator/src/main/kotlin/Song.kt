class Song(private val name: String,
           private val author: String) {

    override fun toString(): String = "$author - $name"
}