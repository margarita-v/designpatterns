package song

interface Song {

    fun play()
}