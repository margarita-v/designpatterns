import singer.PopSinger
import singer.RockSinger
import singer.Singer

fun main(args: Array<String>) {
    createAndPlaySong(RockSinger("Marilyn Manson"))
    createAndPlaySong(PopSinger("Taylor Swift"))
}

private fun createAndPlaySong(singer: Singer) = singer.createSong().play()