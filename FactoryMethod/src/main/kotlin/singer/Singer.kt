package singer

import song.Song

abstract class Singer(val name: String) {

    abstract fun createSong(): Song
}