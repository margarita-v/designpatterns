package singer

import song.PopSong
import song.Song

class PopSinger(name: String): Singer(name) {

    override fun createSong(): Song = PopSong()
}