package singer

import song.RockSong
import song.Song

class RockSinger(name: String): Singer(name) {

    override fun createSong(): Song = RockSong()
}