fun main(args: Array<String>) {
    val database = CachedDatabase()
    printUserInfo(database, 1)
    printUserInfo(database, 2)
    printUserInfo(database, 1)
    printUserInfo(database, 4)
}

fun printUserInfo(databaseInterface: DatabaseInterface, id: Int) {
    val user = databaseInterface.getUser(id)
    val message = if (user != null) user.firstName + ' ' + user.lastName else "User not found"
    println(message)
}