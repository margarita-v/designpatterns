class CachedDatabase: DatabaseInterface {

    private val database = SimpleDatabase()
    private var cachedUsers = ArrayList<User>()

    override fun getUser(id: Int): User? {
        var user = cachedUsers.find { it.id == id }
        if (user != null) {
            return user
        }

        user = database.getUser(id)
        if (user != null) {
            cachedUsers.add(user)
        }
        return user
    }
}