class SimpleDatabase: DatabaseInterface {

    private var users = ArrayList<User>()

    init {
        users.add(User(1, "Name", "Last name"))
        users.add(User(2, "Petr", "Ivanov"))
    }

    override fun getUser(id: Int): User? = users.find { it.id == id }
}