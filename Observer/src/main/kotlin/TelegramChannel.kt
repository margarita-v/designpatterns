class TelegramChannel: Observable {

    private val subscribers = ArrayList<Subscriber>()

    private lateinit var latestNews: String

    fun sendNews(news: String) {
        latestNews = news
        notifySubscribers()
    }

    override fun addSubscriber(subscriber: Subscriber) {
        subscribers.add(subscriber)
        println("$subscriber joined the channel")
    }

    override fun removeSubscriber(subscriber: Subscriber) {
        subscribers.remove(subscriber)
        println("$subscriber left the channel")
    }

    override fun notifySubscribers() = subscribers.forEach { it.getNews(latestNews) }
}