class User(private val firstName: String, private val lastName: String): Subscriber {

    override fun getNews(news: String) = println(toString() + " got the news: $news")

    override fun toString(): String = "$firstName $lastName"
}