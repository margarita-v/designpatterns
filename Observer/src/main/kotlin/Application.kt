fun main(args: Array<String>) {
    val channel = TelegramChannel()

    val user1 = User("Ivan", "Ivanov")
    val user2 = User("Petr", "Sidorov")

    channel.addSubscriber(user1)
    channel.addSubscriber(user2)

    channel.sendNews("Some news")
    channel.removeSubscriber(user2)
    channel.sendNews("Another news")
}