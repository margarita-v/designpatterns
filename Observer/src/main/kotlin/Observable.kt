interface Observable {

    fun addSubscriber(subscriber: Subscriber)

    fun removeSubscriber(subscriber: Subscriber)

    fun notifySubscribers()
}