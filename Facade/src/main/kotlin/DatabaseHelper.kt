class DatabaseHelper {

    private val users: MutableList<User> = ArrayList()

    fun save(email: String, password: String) {
        users.add(User(email, password));
        println("User with email $email was created")
    }

    fun findUser(email: String): User? = users.find { it.email == email }

    fun hasUser(email: String): Boolean = findUser(email) != null
}