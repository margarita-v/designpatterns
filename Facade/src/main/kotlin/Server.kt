class Server {

    private val databaseHelper = DatabaseHelper()
    private val authService = AuthService()
    private val logger = Logger()

    fun signUp(email: String, password: String) {
        logger.log("Sign up")
        if (!databaseHelper.hasUser(email)) {
            databaseHelper.save(email, password)
            logger.log("User signed up successfully")
        } else {
            logger.showError("User with email $email was created earlier")
        }
    }

    fun signIn(email: String, password: String) {
        logger.log("Sign in")
        val user = databaseHelper.findUser(email)
        if (user != null) {
            if (authService.checkAuth(user, password)) {
                logger.log("User logged in successfully")
            } else {
                logger.showError("Invalid password")
            }
        } else {
            logger.showError("User not found")
        }
    }
}