class Logger {

    fun log(message: String) = println(message)

    fun showError(errorMessage: String) = println(errorMessage)
}