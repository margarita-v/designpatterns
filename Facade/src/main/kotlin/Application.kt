fun main(args: Array<String>) {

    val server = Server()

    val testEmail = "hell@hell.com"
    val testPassword = "hellpass"
    val randomString = "random"

    server.signUp(testEmail, testPassword)
    server.signIn(testEmail, testPassword)

    server.signIn(testEmail, randomString)
    server.signIn(randomString, randomString)
}