/**
 * Interface for modern players
 */
interface ModernPlayer {
    fun play(audioType: String, songName: String);
}