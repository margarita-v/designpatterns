class SimplePLayerAdapter(
        private val simplePlayer: SimplePlayer = SimplePlayer()) : ModernPlayer {

    override fun play(audioType: String, songName: String)
            = simplePlayer.play(songName + '.' + audioType)
}