package slider

class AndroidSlider: Slider() {

    override fun startTracking() = println("Start tracking of Android slider")

    override fun stopTracking() = println("Stop tracking of Android slider")
}