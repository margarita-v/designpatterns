package slider

abstract class Slider {

    abstract fun startTracking()

    abstract fun stopTracking()
}