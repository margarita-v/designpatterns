package factory

import buttons.Button
import buttons.IOSButton
import slider.IOSSlider
import slider.Slider
import switch.IOSSwitch
import switch.Switch

class IOSFactory: UIFactory {

    override fun createButton(): Button = IOSButton()

    override fun createSlider(): Slider = IOSSlider()

    override fun createSwitch(): Switch = IOSSwitch()
}