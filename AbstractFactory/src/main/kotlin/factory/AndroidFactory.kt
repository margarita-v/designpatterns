package factory

import buttons.AndroidButton
import buttons.Button
import slider.AndroidSlider
import slider.Slider
import switch.AndroidSwitch
import switch.Switch

class AndroidFactory: UIFactory {

    override fun createButton(): Button = AndroidButton()

    override fun createSlider(): Slider = AndroidSlider()

    override fun createSwitch(): Switch = AndroidSwitch()
}