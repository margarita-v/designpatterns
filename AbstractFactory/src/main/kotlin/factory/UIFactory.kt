package factory

import buttons.Button
import slider.Slider
import switch.Switch

interface UIFactory {

    fun createButton(): Button

    fun createSlider(): Slider

    fun createSwitch(): Switch
}