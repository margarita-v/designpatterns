package buttons

class AndroidButton: Button() {

    override fun push() = println("Android button was pushed")
}