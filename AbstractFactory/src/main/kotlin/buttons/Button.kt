package buttons

abstract class Button {

    abstract fun push()
}