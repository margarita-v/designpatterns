package buttons

class IOSButton: Button() {

    override fun push() = println("iOS button was pushed")
}