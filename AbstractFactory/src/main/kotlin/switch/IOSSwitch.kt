package switch

class IOSSwitch: Switch() {

    override fun check() = println("iOS switch was checked")
}