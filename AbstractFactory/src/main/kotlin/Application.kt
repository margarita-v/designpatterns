import factory.AndroidFactory
import factory.IOSFactory
import factory.UIFactory

fun main(args: Array<String>) {
    createAndLaunchApp(AndroidFactory())
    createAndLaunchApp(IOSFactory())
}

private fun createAndLaunchApp(uiFactory: UIFactory) = MobileApp(uiFactory).launchApp()