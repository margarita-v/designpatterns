import buttons.Button
import factory.UIFactory
import slider.Slider
import switch.Switch

class MobileApp(private val uiFactory: UIFactory) {

    private val button: Button = uiFactory.createButton()
    private val slider: Slider = uiFactory.createSlider()
    private val swicth: Switch = uiFactory.createSwitch()

    fun launchApp() {
        button.push()
        slider.startTracking()
        slider.stopTracking()
        swicth.check()
    }
}