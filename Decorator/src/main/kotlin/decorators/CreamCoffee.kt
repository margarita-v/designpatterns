package decorators

import Coffee

class CreamCoffee(private val coffee: Coffee): Coffee() {

    override fun getCost(): Int = coffee.getCost() + 10

    override fun getDescription(): String = "${coffee.getDescription()} with cream"
}