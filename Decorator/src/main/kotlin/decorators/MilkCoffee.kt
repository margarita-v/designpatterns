package decorators

import Coffee

class MilkCoffee(private val coffee: Coffee): Coffee() {

    override fun getCost(): Int = coffee.getCost() + 5

    override fun getDescription(): String = "${coffee.getDescription()} with milk"
}