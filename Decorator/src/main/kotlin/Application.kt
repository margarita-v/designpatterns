import decorators.CreamCoffee
import decorators.MilkCoffee

fun main(args: Array<String>) {
    val simpleCoffee = SimpleCoffee()
    printCoffeeInfo(simpleCoffee)

    val milkCoffee = MilkCoffee(simpleCoffee)
    printCoffeeInfo(milkCoffee)

    printCoffeeInfo(CreamCoffee(simpleCoffee))
    printCoffeeInfo(CreamCoffee(milkCoffee))
}

private fun printCoffeeInfo(coffee: Coffee) = println(coffee.getDescription())