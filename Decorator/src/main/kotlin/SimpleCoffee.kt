class SimpleCoffee : Coffee() {

    override fun getCost(): Int = 100

    override fun getDescription(): String = "Simple coffee"
}