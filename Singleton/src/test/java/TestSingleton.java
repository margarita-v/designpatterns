import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestSingleton extends Assert {

    private DatabaseHelper databaseHelper;

    @Before
    public void initInstance() {
        databaseHelper = DatabaseHelper.getInstance();
    }

    @Test
    public void testSingleton() {
        DatabaseHelper anotherInstance = DatabaseHelper.getInstance();
        assertEquals(databaseHelper, anotherInstance);
    }

    @After
    public void releaseService() {
        databaseHelper = null;
    }
}
