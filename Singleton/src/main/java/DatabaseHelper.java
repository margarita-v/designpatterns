class DatabaseHelper {

    private DatabaseHelper() { }

    private static DatabaseHelper instance;

    static DatabaseHelper getInstance() {
        if (instance == null) {
            instance = new DatabaseHelper();
        }
        return instance;
    }

    void printDbItems() {
        System.out.println("Database is empty");
    }
}
